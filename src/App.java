import module.Account;

public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("A1", "Nguyen Van Teo");

        Account account2 = new Account("A2", "Nguyen Thi Co", 1000);
        System.out.println("Account 1: " + account1.toString());
        System.out.println("Tang cho Account 1 them 2000: " + account1.credit(2000));
        // System.out.println("Giam Account 1 di 1000: " + account1.debit(1000));
        System.out.println("Chuyen 2000 tu Account 1 den Account 2: " + account1.transferTo(account2,2000));
        System.out.println("Account 1: " + account1.toString());
        System.out.println("Account 2: " + account2.toString());
        System.out.println("Tang cho Account 2 them 3000: " + account2.credit(3000));
        // System.out.println("Giam Account 2 di 5000: " + account1.debit(5000));
        System.out.println("Chuyen 2000 tu Account 2 den Account 1: " + account2.transferTo(account1,2000));
        System.out.println("Account 1: " + account1.toString());
    }
}
